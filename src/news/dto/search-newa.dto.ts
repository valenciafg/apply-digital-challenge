import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsDateString,
  IsOptional,
  IsString,
  MinLength,
} from 'class-validator';
import { PaginationDto } from '../../common/dtos/pagination.dto';

export class SearchNewsDto extends PaginationDto {
  @ApiProperty()
  @IsOptional()
  @IsString()
  @MinLength(1)
  author: string;
  @ApiProperty()
  @IsOptional()
  @IsString()
  @MinLength(1)
  title?: string;
  @ApiProperty()
  @IsOptional()
  // @IsDateString()
  createdAt?: string;
  @ApiProperty()
  @IsString({ each: true })
  @IsArray()
  @IsOptional()
  tags: string[];
}
