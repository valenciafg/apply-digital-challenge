import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsDateString,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
  IsUrl,
  MinLength,
} from 'class-validator';

export class CreateNewsDto {
  @ApiProperty()
  @IsString()
  @MinLength(1)
  author: string;

  @ApiProperty()
  @IsString()
  @MinLength(1)
  objectId: string;

  @ApiProperty()
  @IsNumber()
  createdAtI: number;

  @ApiProperty()
  @IsDateString()
  createdAt: Date;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  @IsPositive()
  storyId?: number;

  @ApiProperty()
  @IsOptional()
  @IsString()
  @MinLength(1)
  storyTitle?: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  @IsUrl()
  storyUrl?: string;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  @IsPositive()
  parentId?: number;

  @ApiProperty()
  @IsOptional()
  @IsString()
  title?: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  @IsUrl()
  url?: string;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  points?: number;

  @ApiProperty()
  @IsOptional()
  @IsString()
  storyText?: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  commentText?: string;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  numComments?: number;

  @ApiProperty()
  @IsString({ each: true })
  @IsArray()
  @IsOptional()
  tags: string[];
}
