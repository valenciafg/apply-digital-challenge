import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class News {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({
    type: 'int',
    nullable: true,
    name: 'story_id',
  })
  storyId: number;
  @Column({
    type: 'varchar',
    nullable: false,
    name: 'object_id',
  })
  objectId: string;
  @Column({
    type: 'text',
    nullable: true,
    name: 'story_title',
  })
  storyTitle: string;
  @Column({
    type: 'text',
    nullable: true,
    name: 'story_url',
  })
  storyUrl: string;
  @Column({
    type: 'int4',
    nullable: true,
    name: 'parent_id',
  })
  parentId: number;
  @Column({
    type: 'text',
    nullable: true,
  })
  title: string;
  @Column({
    type: 'text',
    nullable: true,
  })
  url: string;
  @Column({
    type: 'varchar',
    nullable: false,
  })
  author: string;
  @Column({
    type: 'int4',
    nullable: true,
  })
  points: number;
  @Column({
    type: 'text',
    nullable: true,
    name: 'story_text',
  })
  storyText: string;
  @Column({
    type: 'text',
    nullable: true,
    name: 'comment_text',
  })
  commentText: string;
  @Column({
    type: 'numeric',
    name: 'created_at_i',
  })
  createdAtI: number;
  @Column({
    type: 'date',
    name: 'created_at',
  })
  createdAt: Date;
  @Column({
    type: 'int4',
    name: 'num_comments',
    nullable: true,
  })
  numComments: number;

  @Column({
    type: 'text',
    array: true,
    default: [],
  })
  tags: string[];
}
