import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ArrayContains, Between, Repository } from 'typeorm';
import axios from 'axios';
import * as moment from 'moment';
import { PaginationDto } from 'src/common/dtos/pagination.dto';
import { CreateNewsDto } from './dto/create-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';
import { News } from './entities/news.entity';
import { camelCase } from 'camel-case';
import { Cron, CronExpression } from '@nestjs/schedule';

const months = {
  January: 1,
  February: 2,
  March: 3,
  April: 4,
  May: 5,
  June: 6,
  July: 7,
  August: 8,
  September: 9,
  October: 10,
  November: 11,
  December: 12,
};

@Injectable()
export class NewsService {
  private readonly logger = new Logger(NewsService.name);
  private readonly newsUrl = 'https://hn.algolia.com/api/v1';
  request: any;
  constructor(
    @InjectRepository(News)
    private readonly newsRepository: Repository<News>,
  ) {
    this.request = axios.create({ baseURL: this.newsUrl });
  }
  async create(createNewsDto: CreateNewsDto) {
    try {
      const news = this.newsRepository.create(createNewsDto);
      await this.newsRepository.save(news);
      return news;
    } catch (error) {
      this.handleDBExceptions(error);
    }
  }

  createWhere(filters: any) {
    const where: any = {};

    Object.entries(filters).forEach(([key, value]) => {
      if (key === 'tags') {
        where[key] = ArrayContains(value as string[]);
      }
      if (key === 'title') {
        where[key] = value;
      }
      if (key === 'createdAt') {
        const month = months[value as string];
        if (month) {
          where[key] = Between(
            moment()
              .month(value as string)
              .startOf('month')
              .toDate(),
            moment()
              .month(value as string)
              .endOf('month')
              .toDate(),
          );
        } else {
          const date = moment(value);
          if (date.isValid()) {
            where[key] = date.toDate();
          }
        }
      }
    });
    return where;
  }

  findAll(paginationDto: PaginationDto) {
    const { limit = 10, offset = 0, ...filters } = paginationDto;
    const where = this.createWhere(filters);
    return this.newsRepository.find({
      take: limit,
      skip: offset,
      where,
    });
  }

  remove(id: string) {
    return this.newsRepository.delete({ id });
  }

  handleDBExceptions(error: any) {
    if (error.code && error.detail) {
      throw new BadRequestException(error.detail);
    }
    this.logger.error(error);
    throw new InternalServerErrorException(
      'Unexpected error, check server logs',
    );
  }
  filterDataToDto(hits: any[]) {
    const newData = [];
    for (const hit of hits) {
      const element: any = {};
      Object.entries(hit).forEach(([key, value]) => {
        const newKey = key !== 'objectID' ? camelCase(key) : 'objectId';
        element[newKey] = value;
      });
      const { highlightResult, ...data } = element;
      newData.push(data);
    }
    return newData;
  }

  async saveNewsToDB(data: CreateNewsDto[]) {
    const promises = [];
    for (const element of data) {
      const news = this.newsRepository.create(element);
      promises.push(this.newsRepository.save(news));
    }
    await Promise.all(promises);
  }
  @Cron(CronExpression.EVERY_HOUR, { name: 'load_hacker_news' })
  async loadHackerNews() {
    this.logger.log('loading news...');
    try {
      const {
        data: { hits },
      } = await this.request.get('/search_by_date?query=nodejs');
      const news = this.filterDataToDto(hits);
      await this.saveNewsToDB(news);
      this.logger.log('The news are news loaded!');
    } catch (error) {
      this.logger.error(error.message);
    }
  }
}
