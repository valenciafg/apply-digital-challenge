import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { NewsService } from './news.service';
import { CreateNewsDto } from './dto/create-news.dto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { SearchNewsDto } from './dto/search-newa.dto';

@ApiTags('news')
@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @Post()
  @ApiOperation({
    summary: 'Create a news',
  })
  @UseGuards(AuthGuard())
  create(@Body() createNewsDto: CreateNewsDto) {
    return this.newsService.create(createNewsDto);
  }

  @Get()
  @ApiOperation({
    summary: 'Get a paginated list of news',
  })
  @UseGuards(AuthGuard())
  findAll(@Query() searchNewsDto: SearchNewsDto) {
    return this.newsService.findAll(searchNewsDto);
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'Delete a news by ID',
  })
  remove(@Param('id') id: string) {
    return this.newsService.remove(id);
  }
}
