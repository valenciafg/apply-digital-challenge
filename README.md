<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

## Apply Digital Challenge API
1. Rename .env.example to .env and assign values
2.  DB up
```
docker-compose up -d
```

# Cron job
Create a list of news every hour from external API

# API endpoints
POST /news - Create a news
GET /news - Get a paginated list of news
DELETE /news/{id} - Delete a news from db by ID
POST /auth/register - Register user
POST /auth/login - Login user and get bearer token

# Postman collection
[collection](news.postman_collection.json)